﻿function NewErrorLog(level, errorType, loggedOnDate) {
    return new ErrorLogSearchModel(level, errorType, loggedOnDate);
}

function GetErrorListAsync(page) {
    var filter = NewErrorLog($('#level').val(), $('#error').val(), $('#logDate').val());
    if (page > 0) window.activePage = Number(page);
    $.ajax({
        type: "POST",
        url: "/ErrorLog/ErrorLogList",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("XSRF-TOKEN",
                $('input:hidden[name="__RequestVerificationToken"]').val());
        },
        data: {
            activePage: page,
            filter: filter
        },
        success: function (data) {
            $('#Logs').empty();
            $('#Logs').html(data);
            bindPaginator(filter, page, "/ErrorLog/ErrorLogPage", GetErrorListAsync);
        },
        error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
}

class ErrorLogSearchModel {
    constructor(level, errorType, loggedOnDate) {
        this.Level = level;
        this.ErrorType = errorType;
        this.LoggedOnDate = loggedOnDate;
    }
}