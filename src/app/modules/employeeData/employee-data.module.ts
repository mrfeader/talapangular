import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EmployeeDataComponent} from './components/employee-data.component';
import {NotesTableComponent} from './PageTabs/Notes/notes-table.component';
import {GoalsTableComponent} from './PageTabs/Goals/goals-table.component';
import {ProjectsTableComponent} from './PageTabs/Projects/projects-table.component';
import {UploadTableComponent} from './PageTabs/Documents/documents-table.component';
import {NotificationsTableComponent} from './PageTabs/Notifications/notifications-table.component';
import {SkillsTableComponent} from './PageTabs/Skills/skills-table.component';
import {EmployeeDataRouting} from './employee-data.routing';


@NgModule({
  declarations: [
    EmployeeDataComponent,
    NotesTableComponent,
    GoalsTableComponent,
    ProjectsTableComponent,
    UploadTableComponent,
    NotificationsTableComponent,
    SkillsTableComponent,
  ],
  imports: [
    EmployeeDataRouting,
    CommonModule
  ]
})
export class EmployeeDataModule { }
