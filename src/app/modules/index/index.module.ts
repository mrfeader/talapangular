import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IndexComponent} from './components/index.component';
import {SkillsDropDownComponent} from './components/SkillsDropDown/SkillsDropDown.component';
import {IndexRouting} from './index.routing';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    IndexComponent,
    SkillsDropDownComponent,
  ],
  imports: [
    FormsModule,
    CommonModule,
    IndexRouting
  ]
})
export class IndexModule {
}
