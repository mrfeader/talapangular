import {Component, OnInit} from '@angular/core';
import {OidcSecurityService} from 'angular-auth-oidc-client';
import {AuthenticationService} from './modules/shared/services/authService/auth.service';
import {HttpClientService} from './modules/shared/services/HttpClient/HttpClient.service';
import {HttpResponse} from '@angular/common/http';
import {Router} from '@angular/router';

declare let $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  isAuthenticated: boolean;
  isAuthenticating = true;
  userData: any;
  UserMenuBlock = false;
  title = 'talapangular';

  constructor(
    private auth: AuthenticationService,
    private httpClien: HttpClientService,
    private router: Router) {
    this.auth.$isAuthenticated.subscribe((data: boolean) => this.isAuthenticated = data);
  }

  ngOnInit() {
    if (!this.isAuthenticated && !this.isAuthenticating) {
      this.login();
    }
    $('#userMenuBlock').click(() =>
      $('#userMenu').css('visibility', $('#userMenu').css('visibility') === 'hidden' ? 'visible' : 'hidden')
    );
  }

  login() {
    this.isAuthenticating = true;
    this.auth.login();
    // this.oidcSecurityService.authorize();
  }

  logout() {
    this.auth.logout();
  }
}
