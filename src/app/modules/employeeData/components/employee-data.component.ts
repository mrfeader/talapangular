import { Component, OnInit } from '@angular/core';
import {Guid} from 'guid-typescript';
import {HttpClient} from '@angular/common/http';
import {forkJoin, Observable, Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import { EmployeeService } from '../services/Employee.service';
import { EmployeeAvatarService } from '../services/EmployeeAvatar.service';

import { ModalWindowService } from '../../shared/components/ModalWindow/modal-window.service';
import {map, mapTo, retry} from 'rxjs/operators';

class Employee {
  id: Guid;
  firstName: string;
  lastName: string;
  userName: string;
  positionName: string;
  directionPosition: string;
  dateOfBirth: Date;
  workStartDate: Date;
  avatar: string;
}

@Component({
  selector: 'app-employee-data',
  templateUrl: './employee-data.component.html',
  styleUrls: ['./employee-data.component.scss'],
})

export class EmployeeDataComponent implements OnInit {
  Employee: Employee = new Employee();
  public TabNumber = 0;
  private EmployeeId: Guid;
  closeResult: string;
  private query: Subscription;
  constructor(private http: HttpClient,
              private route: ActivatedRoute,
              private employeeService: EmployeeService,
              private modalWindowService: ModalWindowService,
              private avatarService: EmployeeAvatarService) { }

  ngOnInit() {
    this.route.queryParams.subscribe((param: any) => this.EmployeeId = param.id);
    this.employeeService.GetByIdAsync(this.EmployeeId).subscribe((data: Employee) => this.Employee = data, retry(3));
  }
  ChangeTab(tab: number) {
    this.TabNumber = tab;
  }
  OpenModal() {
    this.modalWindowService.Open(null, null);
  }
}
