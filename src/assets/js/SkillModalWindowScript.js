﻿$(".dotEdit").click(function () {
    switch ($('input[name=skillMark]:checked').val()) {
        case "1":
            $("#EditDot1").css("background", "red");
            $("#EditDot2").css("background", "grey");
            $("#EditDot3").css("background", "grey");
            break;
        case "2":
            $("#EditDot1").css("background", "red");
            $("#EditDot2").css("background", "red");
            $("#EditDot3").css("background", "grey");
            break;
        case "3":
            $("#EditDot1").css("background", "red");
            $("#EditDot2").css("background", "red");
            $("#EditDot3").css("background", "red");
            break;
    }
});

function getJsonCategoris() {
    var s2 = [];
    $.ajax({
        type: "GET",
        url: "/PartialPageTabs/Skills/CategoriesOfSkill",
        data: "{}",
        async: false,
        success: function (categories) {
            $.each(categories, function (index, category) {
                s2.push({ id: category.id, name: category.title });
            });
        },
        error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
    return s2;
};
var MagicForcategory = $("#IdForInputOfEditCategorySkills").magicSuggest({
    data: getJsonCategoris(),
    autoSelect: true,
    maxSelection: 1,
    selectFirst: true,
    placeholder: 'Category'
});

function getJsonSkillsByCategoryName(Name) {
    var s = [];
    $.ajax({
        type: "GET",
        url: "/PartialPageTabs/Skills/SkillsByCategoryName",
        data: {
            id: Name.toString()
        },
        async: false,
        success: function (skills) {
            $.each(skills, function (index, skill) {
                s.push({ id: skill.id, name: skill.name });
            });
        },
        error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
    return s;
};
var MagicForSkills = $("#IdForInputOfEditNameSkills").magicSuggest({
    data: getJsonSkillsByCategoryName(""),
    autoSelect: true,
    cls: 'InputOfSkill',
    maxSelection: 1,
    selectFirst: true,
    placeholder: 'Skill'
});

$(MagicForSkills).on('selectionchange', function () {
    var id = this.getValue();
    $.ajax({
        type: "GET",
        url: "/PartialPageTabs/Skills/SkillById",
        data: {
            SkillId: id.toString()
        },
        success: function (SkillViewModel) {
            if (SkillViewModel != null) {
                MagicForcategory.clear();
                MagicForcategory.setValue([{ id: SkillViewModel.skillCategoryId, name: SkillViewModel.categoryTitle }]);
            }
        },
        error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
});

$("#EmployeeIdForSkillEditPopUp").val($("#HiddenEmployeeId").val());

$("#IdForSubmitOfEditSkills").click(function () {
    var CategoryName;
    var skillName;
    $("#EmployeeSkillHiddenIdForEdit").click();

    if ($("#EmployeeSkillHiddenIdForEdit").val() == "") {
        CategoryName = MagicForcategory.getValue();
        skillName = MagicForSkills.getValue();
        if (MagicForSkills.getValue() === "" || MagicForcategory.getValue() == "") {

            return false;
        }
    } else {
        CategoryName = $("input[name='CategoryName']").val();
        skillName = $("input[name='skillName']").val();
    }

    $.ajax({
        type: "POST",
        url: "PartialPageTabs/Skills/CreateSkill",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("XSRF-TOKEN",
                $('input:hidden[name="__RequestVerificationToken"]').val());
        },
        data: {
            CategoryName: CategoryName,
            EmployeeId: $("#EmployeeIdForSkillEditPopUp").val(),
            skillName: skillName,
            skillMark: $('input[name=skillMark]:checked').val(),
            EmployeeSkillId: $("#EmployeeSkillHiddenIdForEdit").val(),
            IsApproved: $('#IsApprovedSkillSwitch').val()
        },
        success: function (result) {
            $('#MainModal').modal('hide');
            FillSkillsTab(employeeid, '#EmployeeSkills');
            ShowSuccesMessage(result);
        },
        error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
});

$("#IsApprovedSkillButton").on("click", function () {

    $.ajax({
        type: "POST",
        url: "PartialPageTabs/Skills/ApproveEmployeeSkill",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("XSRF-TOKEN",
                $('input:hidden[name="__RequestVerificationToken"]').val());
        },
        data: {    
            EmployeeSkillId: $("#EmployeeSkillHiddenIdForEdit").val(),
            IsApproved: true
        },
        success: function (result) {
            $('#IsApprovedSkillButton').fadeOut();
            $("#IsApprovedSkillSwitch").val("true");
            FillSkillsTab(employeeid, '#EmployeeSkills');
            ShowSuccesMessage(result);
        },
        error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
});