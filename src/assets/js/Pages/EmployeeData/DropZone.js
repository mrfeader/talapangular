﻿$(document).on('dragover drop', function (e) {
    e.preventDefault();
    if (Dropzone.instances.length > 0) {
        var backdrop = $('.backdrop');
        backdrop[0].style.left = '-100%';
        backdrop[0].style.opacity = 0;
        Dropzone.instances[0].previewsContainer.classList.remove("dz-drag-hover");
    }
});
$(document).on('dragover', function (e) {
    if (Dropzone.instances.length > 0) {
        var backdrop = $('.backdrop');
        backdrop[0].style.left = 0;
        backdrop[0].style.opacity = 1;
        Dropzone.instances[0].previewsContainer.classList.add("dz-drag-hover");
    }
});
$(document).on('dragleave', function (e) {
    if (Dropzone.instances.length > 0) {
        var backdrop = $('.backdrop');
        backdrop[0].style.left = '-100%';
        backdrop[0].style.opacity = 0;
        Dropzone.instances[0].previewsContainer.classList.remove("dz-drag-hover");
    }
});

function quickNote(creator, noteID) {
    var title = $('#NoteTitle').val();
    var description = tinyMCE.activeEditor.getContent();
    if (title.length < 1 & description.length < 1) {
        $.ajax({
            type: "POST",
            url: "/PartialPageTabs/Notes/CreateUpdateNote",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            data: {
                id: noteID,
                creatorID: creator,
                EmployeeId: $('#HiddenEmployeeId').val()
            },
            success: function (result) {
                Dropzone.instances[0].options.url = "/PartialPageTabs/Notes/FileAdd?noteID=" + result;
                Dropzone.instances[0].options.headers = {
                    "XSRF-TOKEN":
                        $('input:hidden[name="__RequestVerificationToken"]').val()
                }
                Dropzone.instances[0].processQueue()
                Dropzone.instances[0].on("success", function () {
                    FillNotesTab(employeeid, '#EmployeeNotes');
                    $("#MainModal").modal("hide");
                });
                
            }, error: function (xhr, status, error) {
                if (xhr.status == 500) {
                    ShowErrorMessage("Internal server Error!");
                }
                else {
                    ShowErrorMessage($.parseJSON(xhr.responseText).value);
                }
            }

        });
    }
}
function PostNoteFile(result) {
    Dropzone.instances[0].options.url = "/PartialPageTabs/Notes/FileAdd?noteID=" + result;
    Dropzone.instances[0].options.headers = {
        "XSRF-TOKEN":
            $('input:hidden[name="__RequestVerificationToken"]').val()
    }
    Dropzone.instances[0].processQueue()
    Dropzone.instances[0].on("success", function () {
        FillNotesTab(employeeid, '#EmployeeNotes');

        $('#MainModal').modal('hide');
        Dropzone.instances[0].removeAllFiles();
        Dropzone.instances[0].disable();
    })
    FillNotesTab(employeeid,'#EmployeeNotes');
    $('#MainModal').modal('hide');
}