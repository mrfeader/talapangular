import {Component, Injectable} from '@angular/core';
import {ComponentPortal} from '@angular/cdk/portal';
import {ModalWindowService} from './modal-window.service';

@Injectable({
  providedIn: 'root'
})

@Component({
  selector: 'app-modal-window',
  templateUrl: './modal-window.component.html',
  styleUrls: ['./modal-window.component.scss']
})

export class ModalWindowComponent {

  public get isOpened(): boolean {
    return this.modalWindowService.isOpened;
  }

  public get componentPortal(): ComponentPortal<any> {
    return this.modalWindowService.ComponentPortal;
  }

  constructor(private modalWindowService: ModalWindowService) {
  }

  public dismiss(): void {
    this.modalWindowService.Dismiss();
  }

  // @HostListener('document:keydown', ['$event'])
  // handleKeyBordEvet(event: KeyboardEvent){
  //   if(event.keyCode === 27){
  //     this.isOpened = false;
  //   }
  // }


}
