import {Component, OnInit, Input} from '@angular/core';
import {Guid} from 'guid-typescript';
import { UploadDocumentsService } from '../../services/UploadDocuments.service';
import { DatePipe } from '@angular/common';


class Document {
  id: Guid;
  name: string;
  creationDate: Date;
  description: string;
  creatorId: Guid;
  isPrivate: boolean;
}

@Component({
  selector: 'app-documents-table',
  template: `<div class="mb-2">    
                <div *ngFor="let document of Documents" class="talent-row row col-md-12 m-0 mb-1 p-0 document.id" data-id="document.id" style="height:40px">
                <div class="row m-0 p-0 col-3">
                    <div *ngIf="document.isPrivate" class="col-1 p-2">
                        <div class="svg-image icon-lock"></div>
                    </div>
                    <div [ngClass]="{'col-1':document.isPrivate}" [ngClass]="{'col-2':!document.isPrivate}" class="p-2 m-0">
                        <img src="assets/images/file.svg" />
                    </div>
                    <div class="cut-text col-10 p-2 m-0">
                        <span style="color:crimson">
                        {{GetName(document)}}
<!--                            @(uploadFile.Name.Contains('.') ? uploadFile.Name.Substring(uploadFile.Name.IndexOf('_', uploadFile.Name.IndexOf('_') + 1) + 1, uploadFile.Name.Length - uploadFile.Name.IndexOf('_', uploadFile.Name.IndexOf('_') + 1) - 1) : uploadFile.Name)-->
                        </span>
                    </div>
                </div>
                <div class="cut-text col-6 p-2 m-0 raw">
                    {{document.description}}
                </div>
                <div class="cut-text col-1 p-2 m-0">
                    <span class="font-slim">{{document.creationDate | date:'MM/dd/yyyy'}}</span>
                </div>
                <div class="cut-text col-1 p-2 m-0">
                    <span class="font-slim">
<!--                        @{-->
<!--                            var Bytelength = Convert.ToDouble(new FileInfo(Directory.GetCurrentDirectory() + @"\\wwwroot\\Content\\" + uploadFile.Name).Length);-->
<!--                            var fileSize = "";-->
<!--                             (Bytelength > 1024 * 1024)-->
<!--                            {-->
<!--                                fileSize = Math.Round(Bytelength / (1024 * 1024), 1) + " Mb";-->
<!--                            }-->
<!--                             (Bytelength > 1024)-->
<!--                            {-->
<!--                                fileSize = Math.Round(Bytelength / 1024, 1) + " Kb";-->
<!--                            }-->
<!--                        }-->
<!--                        @fileSize-->
                    </span>
                </div>
                <div class="col-1 row m-0 p-0 edit-delete-group">
                    <div class="col-4 p-2">
<!--                        (System.IO.Path.GetExtension(uploadFile.Name).Contains("jp"))-->
                        
                            <a onclick="showModalImagePV('#filePVModal','/Content/document.name')"><img src="assets/images/ic_search.svg" /></a>
                        
                    </div>
                    <div class="col-4  p-2">
<!--                        <a href="assets/Content/uploadFile.Name" target="_blank" download="uploadFile.Name.Substring(uploadFile.Name.IndexOf('_', uploadFile.Name.IndexOf('_') + 1) + 1)"><img src="~/images/download_D.svg" /></a>-->
                    </div>
<!--                     (Html.GetUserInSystemGuid() == uploadFile.CreatorId | User.IsInRole("ADMIN") | User.IsInRole("MANAGER"))-->
                    
                        <div onclick="DeleteDocument(document.id)" class="col-4 p-2">
                            <img src="assets/images/ic_trash.svg" />
                        </div>
                    
                </div>
            </div>
            </div>`,
  styleUrls: []
})

export class UploadTableComponent implements OnInit {
  @Input() EmployeeId: Guid = Guid.createEmpty();
  public Documents: Document[] = [];

  constructor(private uploadService: UploadDocumentsService, private datePipe: DatePipe) {
  }
  GetName(document: Document){
  return document.name.includes('.') ? document.name.substring(document.name.indexOf('_', document.name.indexOf('_') + 1) + 1, document.name.length - document.name.indexOf('_', document.name.indexOf('_') + 1) - 1) : document.name;
  }
  ngOnInit() {
    this.uploadService.GetUploadsListById(this.EmployeeId).subscribe((data: Document[]) => this.Documents = data)
  }

}
