import {Component, OnInit, Input} from '@angular/core';
import {Guid} from 'guid-typescript';
import {NoteService} from 'src/app/modules/employeeData/services/Note.service';


class Note {
  id: Guid;
  title: string;
  description: string;
  creatorId: Guid;
  creationDate: Date;
}

@Component({
  selector: 'app-notes-table',
  template: `<div class="mb-2">    
                <div class="talent-row row col-md-12 m-0 mb-1 p-0 note.Id" *ngFor="let note of Notes" data-id="note.Id">
                    <div class="col-1 mr-2 row" *ngIf="note.isPrivate">                    
                        <span class="font-slim p-2">
                            {{note.creationDate | date:'MM/dd/yyyy'}}
                        </span>                        
                    </div>
                    <div class="col-1 mr-2 row" *ngIf="!note.isPrivate">
                        <div class="col-4 p-2">
                            <div class="svg-image icon-lock"></div>
                        </div>
                        <div class="col-8 p-2">
                            <span class="font-slim">
                                {{note.creationDate | date:'MM/dd/yyyy'}}
                            </span>
                        </div>                        
                    </div>
                    <div class="cut-text col-2 p-2 m-0">
                        <span>{{note.title}}</span>
                    </div>
                    <div class="cut-text col-8 p-2 m-0 raw">
                        {{note.description}}
                    </div>
                    <div class="col-1 row m-0 p-0 edit-delete-group">
                        <div class="col-8"></div>
<!--                        @if (note.CreatorId == Html.GetUserInSystemGuid() | User.IsInRole("ADMIN") | User.IsInRole("MANAGER"))&ndash;&gt;                        -->
                        <div (click)="DeleteNote(note.Id)" class=" col-4 p-2">
                            <img src="assets/images/ic_trash.svg" />
                        </div>
                        
                    </div>
                </div>
            </div>`,
  styleUrls: []
})

export class NotesTableComponent implements OnInit {
  @Input() EmployeeId: Guid = Guid.createEmpty();
  public Notes: Note[] = [];

  constructor(private noteService: NoteService) {
  }

  ngOnInit() {
    this.noteService.GetNotesListById(this.EmployeeId).subscribe((data: Note[]) => this.Notes = data)
  }

}
