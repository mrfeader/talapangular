import { Injectable } from '@angular/core';
import { HttpClientService } from '../../shared/services/HttpClient/HttpClient.service'
import { FilterModel } from '../models/FilterModel'
import { Observable, throwError } from 'rxjs';
import { Guid } from 'guid-typescript';

@Injectable({
  providedIn: 'root'
})

export class EmployeeSearchService{
  constructor (private httpClient: HttpClientService){}


  GetPartEmployeeTable(page: number, entOnPage: number, asc: boolean = true, orderProperty: string = 'LastName', filter: FilterModel){
    return this.httpClient.getList(`EmployeeSearch/table/${(page-1)*entOnPage}/${entOnPage}?orderProperty=${orderProperty}&asc=${asc}`, this.FillCriteriaString(filter));
  }
  GetEmployeeCountAsync(filter: FilterModel){
    return this.httpClient.getSingle('EmployeeSearch/count?', this.FillCriteriaString(filter));
  }
  FillCriteriaString(filter: FilterModel){
    let url: string='';
    url+= filter.Name!=undefined?filter.Name.trim().length>0?`&FilterQuery=FirstName.ToLower().Contains(@0.ToLower())||LastName.ToLower().Contains(@0.ToLower())||UserName.ToLower().Contains(@0.ToLower())&ValueQuery=${filter.Name}`:'':'';
    url+= filter.FirstName!=undefined?filter.FirstName.trim().length>0?`&FilterQuery=FirstName.Contains(@0)&ValueQuery=${filter.FirstName}`:'':'';
    url+= filter.LastName!=undefined?filter.LastName.trim().length > 0? `&FilterQuery=LastName.Contains(@0)&ValueQuery=${filter.LastName}`:'':'';
    url+= filter.UserName!=undefined?filter.UserName.trim().length > 0 ? `&FilterQuery=UserName.Contains(@0)&ValueQuery=${filter.UserName}`:'':'';

    url+=filter.DateOfBirth!=undefined?new Date(filter.DateOfBirth).toDateString()!='Invalid Date'?`&FilterQuery=DateOfBirth == @0&ValueQuery=${new Date(filter.DateOfBirth).toDateString()}`:'':'';
    url+=filter.WorkStartDate!=undefined?new Date(filter.WorkStartDate).toDateString()!='Invalid Date'?`&FilterQuery=DateOfBirth == @0&ValueQuery=${new Date(filter.WorkStartDate).toDateString()}`:'':'';

    url+=filter.ManagerId!=undefined? filter.ManagerId instanceof Guid?`&FilterQuery=ManagerId==@0&ValueQuery=${filter.ManagerId}`:'':'';
    return url;
  }

}
