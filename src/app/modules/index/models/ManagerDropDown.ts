import { Injectable } from '@angular/core';
import {Guid} from 'guid-typescript';

export class ManagerDropDown {
  id: Guid;
  userName: string;
}
