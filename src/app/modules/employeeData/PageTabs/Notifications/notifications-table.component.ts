import {Component, OnInit, Input} from '@angular/core';
import {Guid} from 'guid-typescript';
import {NotificationsService} from 'src/app/modules/employeeData/services/Notifications.service';
import {ModalWindowService} from '../../../shared/components/ModalWindow/modal-window.service';


class Notification {
  id: Guid;
  title: string;
  description: string;
  expirationTime: Date;
  notifyTime: Date;
  isPrivate: boolean;
  creatorId: Guid;

  // isActive: boolean;
  // isSending: boolean;
  isRepeatable: boolean;
  // repeatableInterval: number;
  // intervalType: number;
}

@Component({
  selector: 'app-notifications-table',
  templateUrl: 'notifications-table.component.html',
  styleUrls: []
})

export class NotificationsTableComponent implements OnInit {
  @Input() EmployeeId: Guid = Guid.createEmpty();
  public Notifications: Notification[] = [];

  constructor(private notificationsService: NotificationsService, private modal: ModalWindowService) {
  }

  ngOnInit() {
    this.notificationsService.GetNotificationListById(this.EmployeeId, 'isActive=true')
      .subscribe((data: Notification[]) => this.Notifications = data);
  }

  OpenModal(notification) {
    this.modal.Open(NotificationModalComponent, notification);
  }

  CheckDate(notification: Notification) {
    if (notification.isRepeatable) {
      return new Date(notification.notifyTime) <= new Date;
    } else {
      return new Date(notification.expirationTime) <= new Date();
    }

  }
}

@Component({
  selector: 'app-notification-modal',
  templateUrl: './notification-modal.component.html',
  styleUrls: []
})

export class NotificationModalComponent {
  // @Input() Notification: Notification = new Notification();
  public get Notification() {
    return this.modalWindowService.entity;
  }

  constructor(private modalWindowService: ModalWindowService) {
  }

  // ngOnInit(): void {
  //   this.Notification =
  // }
}

