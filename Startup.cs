using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Quartz.Spi;
using Swashbuckle.AspNetCore.Swagger;
using TalentApp.DependencyResolver;
using WebApi.Extensions;
using WebApi.Jobs;

namespace WebApi
{
    public class Startup
    {
        private static IContainer Container { get; set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddSession();
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2).AddControllersAsServices();
            //services.AddMvc(options =>
            //{
            //    options.EnableEndpointRouting = false;
            //}).SetCompatibilityVersion(CompatibilityVersion.Version_2_2).AddControllersAsServices();
            //services.AddOData();
            services.AddAntiforgery(o => o.HeaderName = "XSRF-TOKEN");
            services.AddCors();
            services.AddAuthentication(options =>
            {
                options.DefaultScheme = "Cookies";
                options.DefaultAuthenticateScheme = "scheme";
                options.DefaultChallengeScheme = "scheme";
            })
            .AddPolicyScheme("scheme", "oidc or Bearer", options =>
            {
                options.ForwardDefaultSelector = context =>
                {
                    if (!context.Request.Headers.ContainsKey("Authorization"))
                    {
                        return "oidc";
                    }
                    else
                    {
                        return Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme;
                    }
                };
            })
            .AddCookie("Cookies")
            .AddOpenIdConnect("oidc", options =>
                {
                    options.Authority = Configuration.GetSection("AuthServer").Value;
                    options.RequireHttpsMetadata = false;
                    options.CallbackPath = "/signin-oidc";
                    options.ResponseType = "code id_token";
                    options.Scope.Add("openid");
                    options.Scope.Add("profile");
                    options.Scope.Add("email");
                    options.Scope.Add("api1");
                    options.ClientSecret = "secret";
                    options.SignInScheme = "Cookies";
                    options.SignedOutCallbackPath = "/signout-callback-oidc";
                    options.ClientId = "talentApp.elinext.com";
                    options.SaveTokens = true;
                    options.RequireHttpsMetadata = false;
                    options.GetClaimsFromUserInfoEndpoint = true;
                    options.RemoteSignOutPath = "/signout-oidc";
                    options.RefreshOnIssuerKeyNotFound = true;
                    options.UseTokenLifetime = true;
                })
                .AddJwtBearer("Bearer", options =>
                {
                    options.Authority = "http://localhost:51699";
                    options.Audience = "http://localhost:51699/resources";
                    //options.Audience = Configuration.GetSection("AuthServer").Value;
                    options.SaveToken = true;
                    //options.TokenValidationParameters.ValidAudience = "api1";
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                    {
                        ValidAudience = "http://localhost:51699/resources",
                        ValidateAudience = true,
                        ValidIssuer = "http://localhost:51699",
                        ValidateIssuer = true,
                        ValidAudiences = new List<string>{"http://localhost:51699/resources","api1"},
                        ValidateLifetime = true

                    };
                });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1.0", new Info { Title = "Demo Api", Version = "1.0" });
                c.AddSecurityDefinition("oid", new OAuth2Scheme
                {
                    Flow = "HybridAndClientCredentials",
                    AuthorizationUrl = Configuration.GetSection("AuthServer").Value,
                    Scopes = new Dictionary<string, string> { { "talentApp.elinext.com", "Demo Api" } }
                });
                c.OperationFilter<AuthorizeCheckOperationFilter>();
            });
            services.AddTransient<IJobFactory, JobFactory>(provider => new JobFactory(services.BuildServiceProvider()));
            services.AddTransient<EmailSendJob>();
            services.AddTransient<SynchronizationJob>();
            var builder = new ContainerBuilder();
            builder.RegisterAssemblyModules(typeof(Kernel).Assembly);
            builder.Populate(services);
            Container = builder.Build();
            return new AutofacServiceProvider(Container);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }
            app.UseCors(b => b.WithOrigins("http://localhost:5000").AllowAnyMethod().AllowAnyHeader().AllowCredentials());
            app.UseQuartz(quartz =>
            {
                quartz.AddJob<EmailSendJob>("SendEmailJob", "Email",
                    Configuration.GetSection("EmailSenderConfig").GetValue<int>("CheckingIntervalInSeconds"));
                quartz.AddJob<SynchronizationJob>("SynchronizationJob", "Synchronization",
                    Configuration.GetSection("SynchronizationConfig").GetValue<int>("CheckingIntervalInSeconds"));

            });
            app.UseStaticFiles();
            app.UseSession();
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseMvc();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1.0/swagger.json", "Demo Api");
                c.OAuthClientId("talentApp.elinext.com");
                c.OAuthAppName("talentApp.elinext.com");
            });
            //var builder = new ODataConventionModelBuilder();
            //builder.EntitySet<EmployeeViewModel>("Employee");
            //app.UseMvc(routeBuilder =>
            //{
            //    routeBuilder.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
            //    routeBuilder.EnableDependencyInjection();
            //});
            app.UseHttpsRedirection();
            app.Run(async (context) => await Task.Run(() => context.Response.Redirect("/swagger")));
        }
    }
}
