﻿function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
};

function getActiveEmployeesCount() {
    $.ajax({
        type: "GET",
        url: "/Index/EmployeeCount",
        success: function (result) {
            paginatorLength = result;
            carouselPageCount = Math.ceil(result / employeesPerGridPage);
        },
        error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
};

function getEmployeesPart(pageVal, pplPerPage) {
    $.ajax({
        type: "GET",
        url: "/Index/EmployeesPart",
        data: {
            page: pageVal,
            empsPerPage: pplPerPage
        },
        success: function (result) {
            employeeList = result;
            employeesForCarousel = result;

        },
        error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
};

function getActiveEmployees() {
    return $.ajax({
        type: "GET",
        url: "/Index/PageCount"
    });
};

function setChecked(el, checked) {
    if (checked) {
        el.data('checked', 'checked');
    }
    else {
        el.data('checked', 'notChecked');
    }
};

function configure(el, table) {
    if ($(".table-configurator input:checked").length == 0) {
        $(el).prop('checked', true);
    }
    else {
        if (el.data('checked') == 'checked') {
            HideColumn(el.data('value'), table);
            setChecked(el, false);
        }
        else {
            ShowColumn(el.data('value'), table);
            setChecked(el, true);
        }
    }
};

function ShowColumn(name, table) {
    var tds = table.getElementsByClassName(name);
    for (var i = 0; i < tds.length; i++) {
        tds[i].removeAttribute("hidden");
    }
};

function HideColumn(name, table) {
    var tds = table.getElementsByClassName(name);
    for (var i = 0; i < tds.length; i++) {
        tds[i].setAttribute("hidden", '');
    }
};

function ShowPopUpMessage(target, message) {
    $(target).text(message);
    $(target).show();
    $(target).animate({ width: "190px" }, 200);
    $(target).delay(2500).animate({
        visibility: "hidden",
        width: "0px",
    }, 200, function () {
        $(this).hide()
    });
}

function ShowErrorMessage(msg) {
    ShowPopUpMessage('#GeneralErrorBlock', msg);
};
function ShowSuccesMessage(msg) {
    ShowPopUpMessage('#GeneralSuccesBlock', msg);
};

//Delete element by url
function Delete(Id, url) {
    return $.ajax({
        type: "POST",
        url: url,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("XSRF-TOKEN",
                $('input:hidden[name="__RequestVerificationToken"]').val());
        },
        data: {
            Id: Id
        },
        success: function (entity) {
            $("#" + entity.id).remove();
            $("." + entity.id).remove();
            if (typeof entity.title !== "undefined") {
                ShowSuccesMessage("(" + entity.title + ") successfuly removed");
            }
            else {
                ShowSuccesMessage("Element successfuly removed");
            }
        }, error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
};

function DeleteNote(noteID) {
    Delete(noteID, "/PartialPageTabs/Notes/DeleteNote");
};
function DeleteGoal(goalID) {
    Delete(goalID, "/PartialPageTabs/Goals/Delete");
};
function DeleteDocument(documentID) {
    Delete(documentID, "/PartialPageTabs/UploadDocuments/Delete");
};
function DeleteNotification(notificationID) {
    Delete(notificationID, "/PartialPageTabs/Notifications/RemoveNotification");
};
function DeleteSkill(skillID) {
    Delete(skillID, "/PartialPageTabs/Skills/RemoveSkill").then(function (res) {
        if ($("#" + res.categoryId + " .talent-row").length <= 0) {
            $("#" + res.categoryId).remove();
        }
    });
};
function DeleteEmployeeProject(projectID) {
    Delete(projectID, "/PartialPageTabs/EmployeeProject/DeleteEmployeeProject");
};
function DeletePosition(positionID) {
    Delete(positionID, "/Positions/RemovePosition");
};
function DeleteProject(projectId) {
    Delete(projectId, "/PartialPageTabs/Project/RemoveProject").then(function () {
        GetPartialProjectsView(1, "Proj");//Change pages count
    });
};
function LogOut() {
    console.log(2);
    $.ajax({
        type: "GET",
        url: "/Index/LogOut",
        error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
}
$('.close-window').on('click', function () {
    $(this).parents('.message-window').fadeOut(600)
});