import { Injectable } from '@angular/core';
import { HttpClientService } from '../../shared/services/HttpClient/HttpClient.service';
import { Observable, throwError } from 'rxjs';
import { Guid } from 'guid-typescript';

@Injectable({
  providedIn: 'root'
})

export class EmployeeService{
  constructor(private httpClient: HttpClientService){}
  private url = 'Employee/';
  GetByIdAsync(id: Guid) {
    return this.httpClient.getSingle(this.url, id.toString());
  }
  GetEmployeeCountAsync() {

  }
}
