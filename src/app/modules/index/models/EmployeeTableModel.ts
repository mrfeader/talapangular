import { Injectable } from '@angular/core';
import {Guid} from 'guid-typescript';

export class EmployeeTableModel {
  id: Guid;
  firstName: string;
  lastName: string;
  userName: string;
  managerUserName: string;
  dateOfBirth: Date;
  workStartDate: Date;
}
