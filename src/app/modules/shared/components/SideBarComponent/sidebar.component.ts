import {Component, OnInit} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AuthenticationService} from '../../services/authService/auth.service';
import {Guid} from 'guid-typescript';

declare let $: any;

class LoginModel {
  id: Guid;
  firstName: string;
  lastName: string;
  userName: string;
  role: string;
}

@Component({
  selector: 'app-side-bar',
  templateUrl: './sidebar.component.html',
  styleUrls: []
})
export class SidebarComponent implements OnInit {
  private userData: LoginModel;

  constructor(private authService: AuthenticationService) {
    authService.$userData.subscribe((data) => this.userData = data);
  }

  ngOnInit() {
    $('#userMenuBlock').click(() =>
      $('#userMenu').css('visibility', $('#userMenu').css('visibility') == 'hidden' ? 'visible' : 'hidden')
    );
  }

  public logout() {
    this.authService.logout();
  }
}
