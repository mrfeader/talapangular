﻿function ShowModal(modalContent, content) {
    $(modalContent).empty();
    $(modalContent).html(content);
};

//Recieve html content for modal
function GetNoteContent(Id) {
    return $.ajax({
        type: 'GET',
        url: "/PartialPageTabs/Notes/NoteModalWindow",
        data: {
            noteId: Id
        }, error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
};
function GetSkillContent(Id) {
    return $.ajax({
        type: 'GET',
        url: "PartialPageTabs/Skills/SkillModal",
        async: false,
        data: {
            EmployeeSkillId: Id
        }, error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
};
function GetExpiredNotificationContent(NotificationId) {
    return $.ajax({
        type: "GET",
        url: "/ExpiredNotifications/ExpiredNotificationById",
        data: {
            notificationId: NotificationId
        }, error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
};
function GetGoalContent(id) {
    return $.ajax({
        type: "GET",
        url: "/PartialPageTabs/Goals/GoalModal",
        data: {
            GoalId: id
        }, error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
};
function GetUploadsDocumentContent(id) {
    return $.ajax({
        type: 'GET',
        url: "/PartialPageTabs/UploadDocuments/UploadDocumentModal",
        data: {
            documentID: id
        }, error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
};
function GetNotificationContent(Id) {
    return $.ajax({
        type: "GET",
        url: "/PartialPageTabs/Notifications/NotificationById",
        data: {
            id: Id
        }, error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
}; 
function GetDeletingProjectContent(Id) { 
    return $.ajax({
        type: "GET",
        url: "/PartialPageTabs/Project/PartialDeletingPartial",
        data: {
            id: Id
        }, error: function (xhr, status, error) {
            ShowErrorMessage($.parseJSON(xhr.responseText).value);
        }
    });
};
function GetProjectContent(Id) {
    return $.ajax({
        type: "GET",
        url: "/ProjectsData/ProjectModal",
        data: {
            ProjectId: Id
        }, error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
};
function GetImagePreviewModalContent(path) {
    return $.ajax({
        type: "GET",
        data: {
            imgPath: path
        },
        error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        },
        url: "/EmployeeData/ImagePreviewModal"
    });
};
function GetEmployeeProjectContent(id) {
    return $.ajax({
        type: 'GET',
        url: "PartialPageTabs/EmployeeProject/EmployeeProjectModal",
        async: false,
        data: {
            EmployeeProjectId: id
        }, error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
};

function GetErrorLogContent(id) {
    return $.ajax({
        type: 'GET',
        url: "/ErrorLog/ErrorLogById",
        async: false,
        data: {
            id: id
        }, error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
} 

function GetInfoLogContent(id) {
    return $.ajax({
        type: 'GET',
        url: "/InfoLog/InfoLogById",
        async: false,
        data: {
            id: id
        }, error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
}

function GetExpiredGoalContent(id) {
    return $.ajax({
        type: "GET",
        url: "/ExpiredGoals/ExpiredNotificationById",
        data: {
            GoalId: id
        }, error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
};

//Calling modal window for content
function ShowNoteModal(noteID) {
    GetNoteContent(noteID).then(function (result) {
        ShowModal('#ModalContent', result);
        $('#MainModal').modal('show')
    });
};
function ShowSkillModal(EmployeeSkillId) {
    GetSkillContent(EmployeeSkillId).then(function (result) {
        ShowModal('#ModalContent', result);
        $('#MainModal').modal('show')
    });
};
function ShowGoalModal(goalID) {
    GetGoalContent(goalID).then(function (result) {
        ShowModal('#ModalContent', result);
        $('#MainModal').modal('show')
    });
};
function ShowEmployeeProjectModal(EmployeeProjectId) {
    GetEmployeeProjectContent(EmployeeProjectId).then(function (result) {
        ShowModal('#ModalContent', result);
        $('#MainModal').modal('show')
    });
};
function ShowDocumentModal(documentID) {
    GetUploadsDocumentContent(documentID).then(function (result) {
        ShowModal('#ModalContent', result);
        $('#MainModal').modal('show')
    });
};
function ShowNotificationModal(notificationID) {
    GetNotificationContent(notificationID).then(function (result) {
        ShowModal('#ModalContent', result);
        $('#MainModal').modal('show')
    });
};
function ShowErrorLogModal(id) {
    GetErrorLogContent(id).then(function (result) {
        ShowModal('#ModalContent', result);
        $('#MainModal').modal('show');
    });
};

function ShowInfoLogModal(id) {
    GetInfoLogContent(id).then(function (result) {
        ShowModal('#ModalContent', result);
        $('#MainModal').modal('show');
    });
};
function ShowProjectModal(ProjectID) {
    GetProjectContent(ProjectID).then(function (result) {
        ShowModal('#ModalContent', result);
        $('#MainModal').modal('show')
    });
};
function ShowDeletingProjectModal(ProjectID) {
    GetDeletingProjectContent(ProjectID).then(function (result) {
        ShowModal('#ModalContent', result);
        $('#MainModal').modal('show')
    });
};
function showModalImagePV(targetModal, imgPath) {
    $(targetModal).modal('show');
    GetImagePreviewModalContent(imgPath).then(function (result) {
        var targetContent = $(targetModal).children('.modal-dialog').children('.modal-content');
        $(targetContent).html(result);
    });
};
