﻿//Filling functions
function FillNotesTab(employeeid, target) {
    GetUserNotes(employeeid).then(function (result) {
        $(target).html(result);
    });
};
function FillSkillsTab(employeeid, target) {
    GetUserSkills(employeeid).then(function (result) {
        $(target).html(result)
    });
};
function FillGoalsTab(employeeid, target) {
    GetUserGoals(employeeid).then(function (result) {
        $(target).html(result);
    });
};
function FillProjectsTab(employeeid, target) {
    GetUserProjects(employeeid).then(function (result) {
        $(target).html(result)
    });
};
function FillUploadsTab(employeeid, target) {
    GetUserUploads(employeeid).then(function (result) {
        $(target).html(result)
    });
};
function FillNotificationTab(employeeid, target) {
    GetUserNotifications(employeeid).then(function (result) {
        $(target).html(result)
    });
};


//Recieving data
function GetUserNotes(employeeID) {
    return $.ajax({
        type: "Get",
        url: "/PartialPageTabs/Notes/NotesByEmloyeeId",
        data: {
            id: employeeID
        }, error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
};
function GetUserSkills(employeeID) {
    return $.ajax({
        type: "GET",
        url: "/PartialPageTabs/Skills/PartialForSkills",
        data: {
            EmployeeId: employeeID
        }, error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
};
function GetUserGoals(employeeID) {
    return $.ajax({
        type: "GET",
        url: "/PartialPageTabs/Goals/EmployeeGoals",
        data: {
            id: employeeID
        }, error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
};
function GetUserProjects(employeeID) {
    return $.ajax({
        type: "GET",
        url: "/PartialPageTabs/EmployeeProject/PartialForProjects",
        data: {
            EmployeeId: employeeID
        }, error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
};
function GetUserUploads(employeeID) {
    return $.ajax({
        type: "GET",
        url: "/PartialPageTabs/UploadDocuments/EmployeeUploads",
        data: {
            id: employeeID
        }, error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
};
function GetUserNotifications(employeeID) {
    return $.ajax({
        type: "GET",
        url: "/PartialPageTabs/Notifications/AllEmployeeNotifications",
        data: {
            empID: employeeID,
            isActive: true
        },
        error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
};


