import { Injectable } from '@angular/core';
import { HttpClientService } from '../../shared/services/HttpClient/HttpClient.service';
import { Guid } from 'guid-typescript';

@Injectable({
  providedIn: 'root'
})

export class EmployeeAvatarService {
  constructor(private httpClient: HttpClientService) {}
  private url = 'Employee/GetAvatarPath?login=';
  GetByEmailAsync(login: string) {
    return this.httpClient.getSingle(this.url, login, 'https://accounts.elinext.com/');
  }
}
