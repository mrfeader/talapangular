import { Injectable } from '@angular/core';
import { HttpClientService } from '../../shared/services/HttpClient/HttpClient.service';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class SkillService {
  constructor(private httpClient: HttpClientService){}
  private url: string = "skill";

  GetSkillsList(uri){
    return this.httpClient.getList(this.url+uri);
  }
}
