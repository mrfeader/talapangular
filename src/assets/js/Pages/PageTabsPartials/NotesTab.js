﻿function DeleteFile(id) {
    $.ajax({
        type: "GET",
        url: "/PartialPageTabs/Notes/DeleteFile",
        data: {
            ID: id
        },
        success: function () {
            $("div").find("div#" + id).remove();
        }, error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowPopUpMessage('#noteError', "Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }

    });
};

function DeleteComment(id) {
    $.ajax({
        type: "GET",
        url: "/PartialPageTabs/Notes/DeleteComment",
        data: {
            ID: id
        },
        success: function () {
            $("div").find("div#" + id).remove();
            ShowPopUpMessage('#commentSucces', "comment successfully removed");
        }
        , error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowPopUpMessage('#noteError', "Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
};

function GetComments(noteid) {
    $.ajax({
        type: "GET",
        url: "/PartialPageTabs/Notes/CollectComments",
        data: {
            noteID: noteid
        },
        success: function (result) {
            $("#comments").html(result);
            $('textarea.commentArea').val('');
        }, error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowPopUpMessage('#noteError', "Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }

    });
};

function GetFiles(noteid) {
    $.ajax({
        type: "GET",
        url: "/PartialPageTabs/Notes/CollectFiles",
        data: {
            noteID: noteid
        },
        success: function (result) {
            $('#filesForNote').html(result);
        }, error: function (xhr, status, error) {
            ShowErrorMessage($.parseJSON(xhr.responseText).value);
        },
        error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowPopUpMessage('#noteError', "Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }

    });
};

function AddComment(comment, noteid, creator) {
    $.ajax({
        type: "POST",
        url: "/PartialPageTabs/Notes/AddComment",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("XSRF-TOKEN",
                $('input:hidden[name="__RequestVerificationToken"]').val());
        },
        data: {
            commentText: comment,
            noteid: noteid,
            creatorID: creator
        },
        success: function (result) {
            GetComments(noteid);
            ShowPopUpMessage('#noteSuccess', "comment successfully added");
        },
        error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowPopUpMessage('#noteError', "Internal server Error!");
            }
            else {
                ShowPopUpMessage('#noteError', $.parseJSON(xhr.responseText).value);
            }
        }

    });
};