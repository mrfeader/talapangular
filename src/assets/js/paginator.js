﻿var activePage = 1;
var pagesAmount = 1;
var employeeList = [];
var entitiesOnPage = 14;

function bindPaginator(filterModel, current, url, method) {
    $.ajax({
        type: "POST",
        url: url,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("XSRF-TOKEN",
                $('input:hidden[name="__RequestVerificationToken"]').val());
        },
        data: {
            filter: filterModel,
            pageNumber: current,
            entOnPage: entitiesOnPage,
        },
        success: function (result) {
            $("#pagesList").html(result.paginator);
            pagesAmount = result.pagesAmount;
            $(".pagination>.pageBtn").each(function() {
                $(this).on('click',
                    function() {
                        method(this.innerText, entitiesOnPage);
                    });
            });
            $(".prevPageBtn").on('click', function () {
                if (activePage > 1) method(Number(activePage) - 1, entitiesOnPage);
            });
            $(".nextPageBtn").on('click', function () {
                if (activePage + 1 <= pagesAmount) method(Number(activePage) + 1, entitiesOnPage);
            });
        },
        error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
}
