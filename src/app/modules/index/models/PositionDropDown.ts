import { Injectable } from '@angular/core';
import {Guid} from 'guid-typescript';

export class PositionDropDown {
  id: Guid;
  name: string;
}
