import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';


const routes: Routes = [
  {path: '', loadChildren: () => import('./modules/index/index.module').then(m => m.IndexModule)},
  {
    path: 'EmployeeData',
    loadChildren: () => import('./modules/employeeData/employee-data.module').then(m => m.EmployeeDataModule)
  },
  {path: '**', loadChildren: () => import('./modules/index/index.module').then(m => m.IndexModule)},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRouting {
}
