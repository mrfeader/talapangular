import {Injectable} from '@angular/core';
import {Guid} from 'guid-typescript';

export class FilterModel {
  ProjectId?: Guid;
  Name: string;
  PositionId?: Guid;
  SkillId: Guid[];
  ManagerId?: Guid;
  // ManagerUserName;
  LastName: string;
  FirstName: string;
  UserName: string;
  DateOfBirth?: Date;
  WorkStartDate?: Date;
  WorkStartRange?: Date;
  WorkEndRange?: Date;
  IsActive = true;
  FreeEmployeesOnly = true;
}
