import { Component, OnInit, Output, Input } from '@angular/core';
import { SkillDropDown } from '../../models/SkillDropDown'
import { SkillService } from '../../../employeeData/services/Skill.service'

declare let $: any;

@Component({
  selector: 'index-container-skills-dropdown-component',
  template: `<div style="position: relative">                
                <div class="skillsContainer w-100 form-control search-field">
                    <div class="select">
                        <div *ngFor="let skill of selectedSkillsList" class="skill-box">
                            <span>{{skill.name}}</span>
                            <span class="delete-button" (click)="RemoveSkillFromSelection(skill)">+</span>
                        </div>                                           
                    </div>                
                    <input id="skillsFilter" [(ngModel)]="filter" (keyup)="FilterSuggestion()" (dblclick)="DropDownToggle()"> 
                    <div (click)="DropDownToggle()" style="transform: rotate(90deg) scaleY(1.5); margin-left: 10px; cursor: pointer">></div> 
                </div>        
                <div style="display: none" class="dropdown-list dropdown-popup">
                    <div *ngFor="let skill of suggestedSkillsList" (click)="AddSkillToSelection(skill)">{{skill.name}}</div>
                </div>                                                 
            </div>`,
  styleUrls: ['./SkillsDropDown.component.scss']
})

export class SkillsDropDownComponent implements OnInit{
  public filter:string;
  public dropDownShown:boolean=false;
  public skillsList: Array<SkillDropDown> = [];
  @Input() public selectedSkillsList:Array<SkillDropDown> = [];
  public suggestedSkillsList:Array<SkillDropDown> = []
  constructor(private skillService: SkillService){}
  GetSkillsList(){
    this.skillService.GetSkillsList('').subscribe((data:SkillDropDown[])=> {this.skillsList = data; this.suggestedSkillsList=data});
  }
  FilterSuggestion(){
    this.suggestedSkillsList=this.skillsList.filter(item => item.name.toLowerCase().indexOf(this.filter.toLowerCase())>=0)
    $(".dropdown-list").css('display', 'block');
  }
  DropDownToggle(){
    $(".dropdown-list").css('display', $(".dropdown-list").css('display') =='none'? 'block': 'none');
  }
  AddSkillToSelection(skill){
    this.filter='';
    this.DropDownToggle();
    this.selectedSkillsList.push(skill);
    this.skillsList = this.suggestedSkillsList = this.skillsList.filter(item=> item !== skill);
  }
  RemoveSkillFromSelection(skill){
    this.skillsList.push(skill);
    this.suggestedSkillsList = this.skillsList;
    this.selectedSkillsList = this.selectedSkillsList.filter(item=> item !== skill);
  }

  ngOnInit(){
    this.GetSkillsList()

  }
}
