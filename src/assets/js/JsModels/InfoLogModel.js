﻿function NewInfoLog(message, customInfo, userName, loggedOnDate) {
    return new InfoLogSearchModel(message, customInfo, userName, loggedOnDate);
}

function GetInfoListAsync(page) {
    var filter = NewInfoLog($('#message').val(), $('#customInfo').val(), $('#userName').val(), $('#logDate').val());
    if (page > 0) window.activePage = Number(page);
    $.ajax({
        type: "POST",
        url: "/InfoLog/InfoLogList",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("XSRF-TOKEN",
                $('input:hidden[name="__RequestVerificationToken"]').val());
        },
        data: {
            activePage: page,
            filter: filter
        },
        success: function (data) {
            $('#Logs').empty();
            $('#Logs').html(data);
            bindPaginator(filter, page, "/InfoLog/InfoLogPage", GetInfoListAsync);
        },
        error: function (xhr, status, error) {
            if (xhr.status == 500) {
                ShowErrorMessage("Internal server Error!");
            }
            else {
                ShowErrorMessage($.parseJSON(xhr.responseText).value);
            }
        }
    });
}

class InfoLogSearchModel {
    constructor(message, customInfo, userName, loggedOnDate) {
        this.Message = message;
        this.CustomInfo = customInfo;
        this.UserName = userName;
        this.LoggedOnDate = loggedOnDate;
    }
}