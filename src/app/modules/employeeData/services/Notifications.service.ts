import { Injectable } from '@angular/core';
import { HttpClientService } from '../../shared/services/HttpClient/HttpClient.service';
import { Observable, throwError } from 'rxjs';
import { Guid } from 'guid-typescript';

@Injectable({
  providedIn: 'root'
})

export class NotificationsService {
  constructor(private httpClient: HttpClientService){}
  private url: string = "Notification";
  GetNotificationListById(id: Guid, args?:string){
    return this.httpClient.getList(`${this.url}/notifications/${id.toString()}`, `?${args}`);
  }

}
