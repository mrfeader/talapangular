import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {AuthenticationService} from '../authService/auth.service';
import {OidcSecurityService} from 'angular-auth-oidc-client';
import {retry} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class HttpClientService {
  constructor(private httpClient: HttpClient, private authService: AuthenticationService) {
  }

  private apiUrl = 'http://localhost:58101/api/';

  public getList(url: string, args?: string, apiUri: string = this.apiUrl) {
    return this.httpClient.get(`${args !== undefined && args !== 'undefined' ?
      apiUri + url + args :
      apiUri + url}`,
      {headers: {Authorization: `Bearer ${this.authService.getToken()}`}});
  }

  public getSingle(url: string, args?: string, apiUri: string = this.apiUrl) {
    return this.httpClient.get(args !== undefined && args !== 'undefined' ?
      apiUri + url + args :
      apiUri + url, {headers: {Authorization: `Bearer ${this.authService.getToken()}`}});
  }
}
