import {BrowserModule} from '@angular/platform-browser';
import {AppRouting} from './app.routing';
import {AppComponent} from './app.component';
import {SidebarComponent} from './modules/shared/components/SideBarComponent/sidebar.component';
import {HttpClientModule} from '@angular/common/http';
import {
  AuthModule,
  ConfigResult,
  OidcConfigService,
  OidcSecurityService,
  OpenIdConfiguration
} from 'angular-auth-oidc-client';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {DatePipe} from '@angular/common';
import {NotificationModalComponent} from './modules/employeeData/PageTabs/Notifications/notifications-table.component';
import {ModalWindowComponent} from './modules/shared/components/ModalWindow/modal-window.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {PortalModule} from '@angular/cdk/portal';
import {HttpClientService} from './modules/shared/services/HttpClient/HttpClient.service';
import {AuthenticationService} from './modules/shared/services/authService/auth.service';


const oidcConfiguration = './assets/configuration/auth.clientConfiguration.json';

export function loadConfig(oidcConfigService: OidcConfigService) {
  return () => oidcConfigService.load(oidcConfiguration);
}

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    ModalWindowComponent,
    NotificationModalComponent
  ],
  imports: [
    AppRouting,
    HttpClientModule,
    AuthModule.forRoot(),
    BrowserAnimationsModule,
    PortalModule
  ],

  providers: [
    OidcConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: loadConfig,
      deps: [OidcConfigService],
      multi: true,
    },
    BrowserModule,
    FormsModule,
    DatePipe,
    HttpClientService,
    AuthenticationService,
  ],
  entryComponents: [NotificationModalComponent],
  bootstrap: [AppComponent, SidebarComponent]
})

export class AppModule {
  constructor(private oidc: OidcSecurityService, private oidcConfigService: OidcConfigService) {
    this.oidcConfigService.onConfigurationLoaded.subscribe((configResult: ConfigResult) => {

      const config: OpenIdConfiguration = {
        stsServer: 'http://localhost:51699',
        redirect_url: 'http://localhost:5000/callback.html',
        client_id: 'spa',
        response_type: 'id_token token',
        start_checksession: true,
        auto_userinfo: true,
        // storage: localStorage,
        silent_renew: true,
        silent_renew_url: 'http://localhost:5000/silent.html',
        scope: 'api1 email profile openid',
        post_login_route: '/',
        post_logout_redirect_uri: 'http://localhost:5000/index.html',
        // log_console_warning_active: true,
        // log_console_debug_active: true,
      };
      this.oidc.setupModule(config, configResult.authWellknownEndpoints);

      // this.oidc.setCustomRequestParameters({client_secret: 'secret', secret: 'secret'});

    });

  }
}
