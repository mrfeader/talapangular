import { Injectable } from '@angular/core';
import { HttpClientService } from '../../shared/services/HttpClient/HttpClient.service';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class PositionService {
  constructor(private httpClient: HttpClientService){}
  private url: string = "position";
  GetPositionsList(uri){
    return this.httpClient.getList(this.url+uri);
  }

}
