import { Injectable } from '@angular/core';
import {Guid} from 'guid-typescript';

export class SkillDropDown {
  id: Guid;
  name: string;
}
