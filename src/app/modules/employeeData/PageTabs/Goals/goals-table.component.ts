import {Component, OnInit, Input} from '@angular/core';
import {Guid} from 'guid-typescript';
import {GoalService} from 'src/app/modules/employeeData/services/Goal.service';


class Goal {
  id: Guid;
  title: string;
  description: string;
  creatorId: Guid;
  creationDate: Date;
  finishDate: Date;
}

@Component({
  selector: 'app-goals-table',
  template:
    `<div class="mb-2"> 
        <div *ngFor="let goal of Goals" class="talent-row row col-md-12 m-0 mb-1 p-0 @goal.Id" data-id="goal.Id" style="height:40px">
            <div *ngIf="!goal.isPrivate" class=" col-1 mr-2 row">
                <span class="font-slim p-2">
                    {{goal.creationDate | date:'MM/dd/yyyy'}}
                </span>
            </div>
            <div *ngIf="goal.isPrivate" class=" col-1 mr-2 row">        
                <div class="col-4 p-2">
                    <div class="svg-image icon-lock"></div>
                </div>
                <div class="col-8 p-2">
                    <span class="font-slim">
                        {{goal.creationDate | date:'MM/dd/yyyy'}}
                    </span>
                </div>        
            </div>
            <div class="cut-text col-2 p-2 m-0">
              <strong *ngIf="!goal.isComplete && CheckDate(goal.finishDate)" style="color: red;">!</strong> {{goal.title}}
            </div>
            <div class="cut-text col-8 p-2 m-0 raw">
                {{goal.description}}
            </div>
            <div class="col-1 row m-0 p-0 edit-delete-group">
                <div class="col-8"></div>
<!--    @if (Html.GetUserInSystemGuid() == goal.CreatorId | User.IsInRole("ADMIN") | User.IsInRole("MANAGER"))-->
                <div onclick="DeleteGoal('@goal.Id')" class="col-4 p-2">
                    <img src="assets/images/ic_trash.svg" />
                </div>
            </div>
        </div>
    </div>`,
  styleUrls: []
})

export class GoalsTableComponent implements OnInit {
  @Input() EmployeeId: Guid = Guid.createEmpty();
  public Goals: Goal[] = [];

  constructor(private goalService: GoalService) {
  }

  ngOnInit() {

    this.goalService.GetGoalsListById(this.EmployeeId).subscribe((data: Goal[]) => this.Goals = data)
  }

  CheckDate(date: Date) {
    return new Date(date) <= new Date();
  }
}




