import { Injectable } from '@angular/core';
import { HttpClientService } from '../../shared/services/HttpClient/HttpClient.service';
import { Observable, throwError } from 'rxjs';
import { Guid } from 'guid-typescript';

@Injectable({
  providedIn: 'root'
})

export class SkillCategoryService {
  constructor(private httpClient: HttpClientService){}
  private url: string = "SkillCategory";
  // GetNotificationListById(id: Guid, args?:string){
  //   return this.httpClient.getList(`${this.url}/notifications/${id.toString()}`, `?${args}`);
  // }
  GetCategoryById(id: Guid){
    return this.httpClient.getSingle(`${this.url}/`, id.toString());
  }

}
