import {Component, OnInit, Input} from '@angular/core';
import {Guid} from 'guid-typescript';
import {EmployeeProjectService} from '../../services/EmployeeProject.service';
import { DatePipe } from '@angular/common';


class Project {
  id: Guid;
  startOfWorkDate: Date;
  endOfWorkDate: Date;
  projectId: Guid;
  projectTitle: string;
}

@Component({
  selector: 'app-projects-table',
  template: `<div class="mb-2">    
                <div *ngFor="let project of Projects" class="talent-row row col-md-12 m-0 mb-1 p-0 project.Id" data-id="project.Id" style="height:40px">
                    <div class="row col-4 p-2 m-0">
                        <div class="col-1 p-0">
                            <div [ngClass]="{'icon-lock':project.isPrivate}" class="svg-image"></div>
                        </div>
                        <div class="col-5 p-0 m-0">
                            <span class="font-slim">{{GetDate(project.startOfWorkDate)}}</span>
                        </div>
                        <div class="col-1 p-0 m-0 text-center">
                            <span class="font-slim">
                                <strong>-</strong>
                            </span>
                        </div>
                        <div class="col-5 p-0 m-0">
                            <span class="font-slim">{{GetDate(project.endOfWorkDate)}}</span>
                        </div>
                    </div>
                    <div class="cut-text col-7 p-2 m-0">
                        <span>{{project.projectTitle}}</span>
                    </div>
                    <div class="col-1 row m-0 p-0 edit-delete-group">
                        <div class="col-8"></div>
<!--                        @if (User.IsInRole("ADMIN") | User.IsInRole("MANAGER"))-->                        
                            <div onclick="DeleteEmployeeProject(project.id)" class="col-4 p-2">
                                <img src="assets/images/ic_trash.svg" />
                            </div>
                        
                        </div>
                </div>
            </div>`,
  styleUrls: []
})

export class ProjectsTableComponent implements OnInit {
  @Input() EmployeeId: Guid = Guid.createEmpty();
  public Projects: Project[] = [];

  constructor(private projectService: EmployeeProjectService, private datePipe: DatePipe) {
  }
  GetDate(date){

    return date!==null? this.datePipe.transform(new Date(date), 'MM/dd/yyyy') : 'None'
  }
  ngOnInit() {
    this.projectService.GetEmployeeProjectListById(this.EmployeeId).subscribe((data: Project[]) => this.Projects = data)
  }

}
