import {Component, OnInit, NgModule, Input} from '@angular/core';
import {Guid} from 'guid-typescript';
import {Route, Router} from '@angular/router';
import { OidcSecurityService } from 'angular-auth-oidc-client';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { UserManager, User } from 'oidc-client';
import { EmployeeTableModel } from '../models/EmployeeTableModel'
import { ManagerDropDown } from '../models/ManagerDropDown'
import { FilterModel } from '../models/FilterModel'
import { EmployeeSearchService } from '../services/EmployeeSearch.service'
import { ManagerService } from '../services/Manager.service';
import { PositionService } from '../../employeeData/services/Position.service';
import { SkillService } from '../../employeeData/services/Skill.service';
import { PositionDropDown } from '../models/PositionDropDown';
import { SkillDropDown } from '../models/SkillDropDown';

declare let $: any;
@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  providers: [OidcSecurityService]
})

export class IndexComponent implements OnInit {
  Employees: EmployeeTableModel[] = [];
  Managers: ManagerDropDown[] =[];
  Positions: PositionDropDown[] = [];
  @Input() Skills: SkillDropDown[] = [];
  isAuthenticated:boolean;
  token: string;
  PagesAmount: number = 1;
  CurrentPage: number=1;
  paginator: number[] = [];
  filter: FilterModel = new FilterModel();
  timeout = null
  AdvancedSearch:boolean = false;
  TableSettings:boolean = false;
  headerConfig={
    FirstName: true,
    LastName: true,
    BirthDay: true,
    UserName: true,
    WorkStart: true,
    Manager: true
  }

  constructor(private  router: Router,
              public oidcSecurityService: OidcSecurityService,
              private employeeSearchService : EmployeeSearchService,
              private managerService: ManagerService,
              private positionService: PositionService,
              private skillService: SkillService) {  }
  EmployeePage(id: Guid) {
    this.router.navigate(['EmployeeData'], {queryParams: { 'id': id}});
  }
  foo(){
    console.log(this.filter)
  }
  BuildPaginator(){
    var pages: number[] = new Array();
    if (this.PagesAmount < 2) return [];
    if (this.CurrentPage>=4 && this.PagesAmount>6) pages.push(1,0);

    let startCounter = this.CurrentPage - 1;
    let end = this.CurrentPage + 1;
    if (this.CurrentPage < 4)
    {
      startCounter = 1;
      end = 5;
    }
    if(this.CurrentPage > this.PagesAmount-3 && this.CurrentPage > 3)
    {
      startCounter = this.PagesAmount - 4;
      end = this.PagesAmount;
    }
    if (this.PagesAmount < 7)
    {
      startCounter = 1;
      end = this.PagesAmount;
    }
    for (let start = startCounter; start <= end && start <= this.PagesAmount; start++)
    {
      pages.push(start);
    }
    if(this.CurrentPage < this.PagesAmount - 2 && this.PagesAmount > 6)
    pages.push(0, this.PagesAmount);
    return pages;
  }
  ChangePage(page: number){
    clearTimeout(this.timeout);
    this.timeout = setTimeout(()=> {
      this.FilterEmployees(page);
    },1000)
  }
  TableSettingsPopup(){
    this.TableSettings=this.TableSettings==true?false:true;
  }
  FilterEmployees(page:number){
    var result = this.Employees;
    if(page>0 && page<=this.PagesAmount){

    this.filter.SkillId =this.AdvancedSearch == true? this.Skills.map(x=> x.id): [];
    this.employeeSearchService.GetPartEmployeeTable(page, 14, true, 'LastName', this.filter).subscribe((data:EmployeeTableModel[])=> this.Employees = data);

    this.employeeSearchService.GetEmployeeCountAsync(this.filter).subscribe((data:number)=>
    {
      this.PagesAmount = Math.ceil(data/14)
        this.CurrentPage=page;
        this.paginator = this.BuildPaginator();
    })
    }

  }
  AdvancedSearchToggle(){
    this.AdvancedSearch=!this.AdvancedSearch;
  }
  isCurrentPage(page){
    return page===this.CurrentPage;
  }
  GetManagersList(){
    this.managerService.GetManagersList('').subscribe((data:ManagerDropDown[]) => this.Managers = data);
  }
  GetSkillsList(){
    //this.skillService.GetSkillsList('').subscribe((data:SkillDropDown[])=> this.Skills = data);
  }
  GetPositionsList(){
    this.positionService.GetPositionsList('').subscribe((data: PositionDropDown[])=> this.Positions = data);
  }
  ngOnInit() {
    this.FilterEmployees(1);
    this.GetManagersList();
    this.GetPositionsList();
    $(document).ready(function () {
      $(".datepicker").datepicker()
    })
    //this.oidcSecurityService.getUserData().subscribe(data=> console.log(data))
    this.GetSkillsList();
  }


}
