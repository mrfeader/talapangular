import {OpenIdConfiguration, ConfigResult, OidcConfigService, OidcSecurityService} from 'angular-auth-oidc-client';
import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Guid} from 'guid-typescript';

const oidcConfiguration = '../../../../assets/configuration/auth.clientConfiguration.json';

export function loadConfig(oidcConfigService: OidcConfigService) {
  return () => oidcConfigService.load(oidcConfiguration);
}

class LoginModel {
  id: Guid;
  firstName: string;
  lastName: string;
  userName: string;
  role: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private userData: any;
  private user: LoginModel;
  public $isAuthenticated = new BehaviorSubject(false);
  public $userData = new BehaviorSubject(null);

  public get getUserData() {
    return this.userData;
  }

  constructor(private oidcSecurityService: OidcSecurityService) {
    this.oidcSecurityService.getUserData().subscribe((data) => {
      this.user = {
        id: data.sub,
        role: data['http://schemas.microsoft.com/ws/2008/06/identity/claims/role'],
        userName: data['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress'],
        firstName: data['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name'],
        lastName: ''
      };
      this.$userData.next(this.user);
    });
    this.oidcSecurityService.getIsAuthorized().subscribe((data) => {
      this.$isAuthenticated.next(data);
    });
    if (this.oidcSecurityService.moduleSetup) {
      this.doCallbackLogicIfRequired();
    } else {
      this.oidcSecurityService.onModuleSetup.subscribe(() => {
        this.doCallbackLogicIfRequired();
      });
    }
  }

  private doCallbackLogicIfRequired() {
    // Will do a callback, if the url has a code and state parameter.
    this.oidcSecurityService.authorizedCallbackWithCode(window.location.toString().substr(1));
  }

  public login() {
    this.oidcSecurityService.authorize();
  }

  public logout() {
    this.oidcSecurityService.logoff();
  }

  getToken() {
    return this.oidcSecurityService.getToken();
  }
}

