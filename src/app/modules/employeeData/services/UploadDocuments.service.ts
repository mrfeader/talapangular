import { Injectable } from '@angular/core';
import { HttpClientService } from '../../shared/services/HttpClient/HttpClient.service';
import { Observable, throwError } from 'rxjs';
import { Guid } from 'guid-typescript';

@Injectable({
  providedIn: 'root'
})

export class UploadDocumentsService {
  constructor(private httpClient: HttpClientService){}
  private url: string = "UploadFile";
  GetUploadsListById(id: Guid){
    return this.httpClient.getList(`${this.url}/employee/`, id.toString());
  }

}
