import { Injectable } from '@angular/core';
import { HttpClientService } from '../../shared/services/HttpClient/HttpClient.service';
import { Observable, throwError } from 'rxjs';
import { Guid } from 'guid-typescript';

@Injectable({
  providedIn: 'root'
})

export class NoteService {
  constructor(private httpClient: HttpClientService){}
  private url: string = "Note";
  GetNotesListById(id: Guid){
    return this.httpClient.getList(`${this.url}/employee/`, id.toString());
  }

}
