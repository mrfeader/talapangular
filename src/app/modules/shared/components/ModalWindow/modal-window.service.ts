import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { ModalWindowComponent } from './modal-window.component';
import { ComponentPortal } from '@angular/cdk/portal';

@Injectable({
  providedIn: 'root'
})

export class ModalWindowService {
  public isOpened = false;
  ComponentPortal: ComponentPortal<any>;
  public entity: any;
  Toggle() {
    this.isOpened = !this.isOpened;
    console.log(this.isOpened);
  }
  Open(content, entity) {
    this.entity = entity;
    this.isOpened = true;
    this.ComponentPortal = new ComponentPortal(content);
  }
  Dismiss() {
    this.isOpened = false;
  }
  constructor(){}

}
