import {Component, OnInit, Input} from '@angular/core';
import {Guid} from 'guid-typescript';
import {EmployeeSkillsService} from '../../services/EmployeeSkills.service';
import {SkillCategoryService} from '../../services/SkillCategory.service';
import {DatePipe} from '@angular/common';

class Skill {
  id: Guid;
  mark: number;
  isApproved: boolean;
  skillId: Guid;
  skillName: string;
  categoryId: Guid;
}

class SkillCategory {
  id: Guid;
  title: string;
  SkillsList: Skill[] = [];
}

@Component({
  selector: 'app-skills-table',
  templateUrl: './skills-table.component.html',
  styleUrls: []
})

export class SkillsTableComponent implements OnInit {
  @Input() EmployeeId: Guid = Guid.createEmpty();
  public Skills: Skill[] = [];
  public SkillCategories: SkillCategory[] = [];

  constructor(private skillsService: EmployeeSkillsService, private categoryService: SkillCategoryService) {
  }

  ngOnInit() {
    this.skillsService.GetSkillsListById(this.EmployeeId).subscribe((data: Skill[]) => {
      this.Skills = data;
      this.Skills.map(x => x.categoryId)
        .filter((el, index, self) => index === self.indexOf(el))
        .forEach(el => {
          this.categoryService.GetCategoryById(el).subscribe((data: { id: Guid, title: string }) => {
            this.SkillCategories.push(
              {
                id: data.id,
                title: data.title,
                SkillsList: this.Skills.filter(skill => skill.categoryId === data.id).sort((a, b) => a.mark - b.mark)
              }
            )
          })
        });
    })
  }
}
